import reporter from 'cucumber-html-reporter';

const options = {
  theme: 'bootstrap',
  ignoreBadJsonFile: true,
  jsonDir: 'html-report/input/',
  output: 'html-report/output/report.html',
  screenshotsDirectory: 'cypress/screenshots/',
  storeScreenshots: true,
  brandTitle: 'Acceptance Tests Report',
  reportSuiteAsScenarios: true,
  scenarioTimestamp: true,
  launchReport: true,
  columnLayout: 1
};

reporter.generate(options);
