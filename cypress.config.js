import { defineConfig } from "cypress";
import createBundler from "@bahmutov/cypress-esbuild-preprocessor";
import addCucumberPreprocessorPlugin from "@badeball/cypress-cucumber-preprocessor/lib/add-cucumber-preprocessor-plugin.js";
import {createEsbuildPlugin} from "@badeball/cypress-cucumber-preprocessor/esbuild.js";

import dotenv from 'dotenv'

const addCucumberPreprocessor = addCucumberPreprocessorPlugin.default

dotenv.config()

const DEFAULT_BASE_URL = 'http://localhost:8080'
const DEFAULT_GRAPHQL_ENDPOINT = '/bff/graphql'

export default defineConfig({
  e2e: {
    specPattern: "**/*.feature",
    async setupNodeEvents(on, config) {
      // This is required for the preprocessor to be able to generate JSON reports after each run, and more,
      await addCucumberPreprocessor(on, config);

      on(
        "file:preprocessor",
        createBundler({
          plugins: [createEsbuildPlugin(config)],
        })
      );

      /**
       * existing env have to be kept
       * => contains preprocessor configuration
       */
      config.env = {
        ...config.env,
        graphqlEndpoint: process.env.GRAPHQL_ENDPOINT || DEFAULT_GRAPHQL_ENDPOINT,
        baseUrl: process.env.BASE_URL || DEFAULT_BASE_URL,
        bu: process.env.BU,
        oAuthToken: process.env.API_OAUTH3S_TOKEN,
        apiKey: process.env.API_KEY,
      }

      config.screenshotsFolder = "cypress/output/screenshots"
      config.videosFolder = "cypress/output/videos"


      // modify config values
      config.defaultCommandTimeout = 10000
      
      // IMPORTANT return the updated config object
      return config

    },
  },
});
