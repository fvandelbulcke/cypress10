const hasOperationName = (req, operationName) => {
  const { body } = req
  return body.hasOwnProperty('operationName') && body.operationName === operationName
}

Cypress.Commands.add('interceptGraphqlOperations', (operations) => {
  return cy.intercept('POST', Cypress.env('graphqlEndpoint'), (req) => {
    for (const operationName of operations) {
      if (hasOperationName(req, operationName)) {
        req.alias = operationName
      }
    }
  })
})

Cypress.Commands.add('sortArrayBy', (array, prop) => {
  return cy.wrap(array.sort(GetSortOrder(prop)))
})

function GetSortOrder(prop) {
  return function (a, b) {
    if (a[prop] > b[prop]) {
      return 1
    } else if (a[prop] < b[prop]) {
      return -1
    }
    return 0
  }
}