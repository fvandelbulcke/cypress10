// ***********************************************************
// This example support/e2e.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

Cypress.on("window:before:load", window => {
  window.tc_privacy_used = 1
});

//uncaught exception to uncaught issues on cy.visit()
Cypress.on('uncaught:exception', (err, runnable) => {
  return false
})

// Attach screeshots to cucumber reports
afterEach(() => {
  const screenshotsFolder = Cypress.config('screenshotsFolder')
  if (window.json?.enabled) {
    const testState = window.testState
    const stepResult = testState.runTests[testState.currentScenario.name][testState.currentStep]
    if (stepResult?.status === 'failed') {
      const screenshotFileName = `${testState.feature.name} -- ${testState.currentScenario.name} (failed).png`
      const screenshotFilePath = `${screenshotsFolder}/${Cypress.spec.name}/${screenshotFileName}`
      cy.task('readFileMaybe', { filename: screenshotFilePath, encoding: 'base64' }).then((imgData) => {
        if (imgData) {
          stepResult.attachment = {
            data: imgData,
            media: { type: 'image/png' },
            index: testState.currentStep,
            testCase: testState.formatTestCase(testState.currentScenario),
          }
        }
      })
    }
  }
})
