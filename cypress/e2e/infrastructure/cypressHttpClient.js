import { getAuthorization} from './authentication'

const executeRequest = async (method, url, body = null, options = {}) => {
  return getAuthorization()
    .then((authorizationHeader) => {
      return cy.request({
        method,
        url,
        body,
        failOnStatusCode: options.canFail,
        headers: {
          Authorization: authorizationHeader,
          apikey: Cypress.env('apiKey'),
        },
      })
    })
};

const get = (path) => executeRequest('GET', path);
const put = (path, body, options) => executeRequest('PUT', path, body, options);

export default {
  get,
  put,
}