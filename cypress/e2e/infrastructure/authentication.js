export const authenticate = () => {
  cy.request({
    method: 'POST',
    url: 'https://oauth3s-preprod.np.digiforce.io/oauth3s/oauth/token?grant_type=client_credentials',
    body: null,
    headers: {
      Authorization: `Basic ${Cypress.env('oAuthToken')}`,
    },
  }).as('authenticateResponse')
}

export const getAuthorization = () => {
  return cy.get('@authenticateResponse')
    .then((token) => `${token.body.token_type} ${token.body.access_token}`)
}
