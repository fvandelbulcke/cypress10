import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";


Given('I am in the Home Page', function () {
  cy.visit(Cypress.env('baseUrl'));
  cy.wait(100)
})

When("I visit google", () => {
  cy.visit('https://www.google.fr/');
});

Then('I should see the google logo', () => {
  cy.get(".lnXdpd").should('exist');
})
