import { Given, When, Then, And, Step  } from "@badeball/cypress-cucumber-preprocessor";


Given('I search norauto', function () {
  Step(this, 'I visit google')
  Step(this, 'I refuse cookies')
  Step(this, 'I type norauto')
})

And("I refuse cookies", () => {
  cy.get('button#W0wltc').click()
});

And("I type norauto", () => {
  cy.get('input.gLFyf').type("norauto{enter}");
});

Then('search results are displayed', () => {
  cy.get("div.hlcw0c").should('exist');
})
